﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class CollisionHandler : System.Object
{
    protected CharacterHadler Character;

    public CollisionHandler(CharacterHadler character)
    {
        Character = character;
    }

    public virtual void Enter()
    {
        MonoBehaviour.print("Enter " + this);
    }

    public virtual void Exit()
    {
        MonoBehaviour.print("Exit " + this);
    }
}

class GroundCollisionHandler : CollisionHandler
{
    public GroundCollisionHandler(CharacterHadler character) : base(character)
    {
    }

    public override void Enter()
    {

        
        Character.State.Grounded();
        Character.JumpsLeft = 3;
        
        base.Enter();
    }

    public override void Exit()
    {
        Character.State.ToInAirState();
        base.Exit();
    }
}

class WallCollisionHandler : CollisionHandler
{
    public WallCollisionHandler(CharacterHadler character) : base(character)
    {
    }

    public override void Enter()
    {
        Character.State.WallCollided(true);
        Character.JumpsLeft = 3;
        base.Enter();
    }

    public override void Exit()
    {
        Character.State.WallCollided(false);
        base.Exit();
    }
}

