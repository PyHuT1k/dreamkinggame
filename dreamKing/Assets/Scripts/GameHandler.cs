﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameHandler : MonoBehaviour
{
    public GameObject Player;
    public KeyStates keyStates;
    // Start is called before the first frame update
    private float airSpeed;
    void Start()
    {
        Debug.Log("GameHandler start");
    }


    void Run(Animator thisAnim)
    {
        thisAnim.ResetTrigger("Stay");
        thisAnim.SetTrigger("TrRun");
        print("TrRunLeft In ");
    }

    void Stay(Animator thisAnim)
    {
        thisAnim.ResetTrigger("TrRun");
        thisAnim.SetTrigger("Stay");
        print("Stay");
    }

    void Jump(Animator thisAnim)
    {
        thisAnim.ResetTrigger("TrRun");
        thisAnim.ResetTrigger("Stay");
        thisAnim.SetTrigger("TrJump");
        thisAnim.SetBool("JumpStarted", true);
        print("Jump");
    }

    // Update is called once per frame
    void Update()
    {

        CharacterHadler handler = Player.GetComponent<CharacterHadler>();

        if (keyStates.Left )
        {
            handler.State.MoveLeft();
        }
        else if (keyStates.Right)
        {
            handler.State.MoveRight();
        }
        else if (!keyStates.Right && !keyStates.Left)
        {
            handler.State.StopMove();
        }


        if (Input.GetKeyUp(KeyCode.W))
        {
            handler.State.Jump();
            return;
        } 


    }
}
