﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyStates : MonoBehaviour
{

    bool Up;
    public bool Right;
    public bool Left;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            print("KeyDown(KeyCode.A)");
            Left = true;
            Right = false;
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            print("KeyDown(KeyCode.D)");
            Left = false;
            Right = true;
        }

        if (Input.GetKeyUp(KeyCode.D))
        {
            Right = false;
        }

        if (Input.GetKeyUp(KeyCode.A))
        {
            Left = false;
        }
    }
}
