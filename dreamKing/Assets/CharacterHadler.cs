﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CharacterHadler : MonoBehaviour, ChracterStateOwner
{
    public ChState current;
    public float verticalSpeed;
    public bool isGrounded;
    private ChracterState _state;

    private Dictionary<string, CollisionHandler> collisionHandlers = new Dictionary<string, CollisionHandler>();
    
    public int JumpsLeft;
    public ChracterState State {
        get { return _state; }
        set
        {
            if (_state != null)
            {
                _state.Exit();
            }
           _state = value;
           _state.Enter();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        State = ChracterState.CreateChracterState(this, ChState.Idle);
        collisionHandlers.Add("wall", new WallCollisionHandler(this));
        collisionHandlers.Add("ground", new GroundCollisionHandler(this));
    }

    // Update is called once per frame
    void Update()
    {
        State.Update();
        Rigidbody2D rigidbody = this.gameObject.GetComponent<Rigidbody2D>();
        verticalSpeed = rigidbody.velocity.y;

        Animator thisAnim = this.gameObject.GetComponent<Animator>();
        thisAnim.SetFloat("VerticalSpeed", verticalSpeed);

    }

    void OnCollisionEnter2D(Collision2D col)
    {
        print("OnCollisionEnter2D" + col.gameObject.tag);
        collisionHandlers[col.gameObject.tag].Enter();
    }

    void OnCollisionExit2D(Collision2D col)
    {
        print("OnCollisionExit2D" + col.gameObject.tag);
        collisionHandlers[col.gameObject.tag].Exit();
    }

    public GameObject Character()
    {
        return this.gameObject;
    }

    public void ChangeState(ChState newState)
    {
        switch (newState)
        {
            case ChState.Jump:
                JumpsLeft--;
                if (JumpsLeft < 0)
                {
                    return;
                }
                break;
        }
        State = ChracterState.CreateChracterState(this, newState);
    }
}
