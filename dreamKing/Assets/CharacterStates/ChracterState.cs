﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum ChState
{
    Idle,
    RunLeft,
    RunRight,
    Jump,
    InAir,
    WallGlide
}

public interface ChracterStateOwner
{
    GameObject Character();
    void ChangeState(ChState newState);
 
}

abstract public class ChracterState : System.Object
{
    protected ChracterStateOwner Owner;
    readonly static public ChState state;

    protected Animator CharacterAnimator()
    {
        return Owner.Character().GetComponent<Animator>();
    }

    static public ChracterState CreateChracterState(ChracterStateOwner owner, ChState state)
    {
        MonoBehaviour.print("CreateChracterState " + state);
        switch (state) {
            case ChState.RunLeft:
                return new ChracterStateRunLeft(owner);
            case ChState.RunRight:
                return new ChracterStateRunRight(owner);
            case ChState.InAir:
                return new ChracterStateInAir(owner);
            case ChState.Jump:
                return new ChracterStateJump(owner);
            case ChState.Idle:
                return new ChracterStateIdle(owner);
            case ChState.WallGlide:
                return new ChracterStateWallGlide(owner);
        }

        throw new NotImplementedException();
        
    }



    public virtual void Update()
    {
       
    }

    public ChracterState(ChracterStateOwner StateOwner)
    {
        Owner = StateOwner;
    }
    public virtual void Enter()
    {
        MonoBehaviour.print("Enter " + this);
    }

    public virtual void Exit()
    {
        MonoBehaviour.print("Exit " + this);
    }

    public virtual void Jump()
    {

    }

    public virtual void MoveLeft()
    {

    }

    public virtual void MoveRight()
    {

    }

    public virtual void StopMove()
    {

    }

    public virtual void Grounded()
    {
        MonoBehaviour.print("Grounded " + this);
    }

    public virtual void ToInAirState()
    {
        Owner.ChangeState(ChState.InAir);
    }

    public virtual void WallCollided(bool isCollided)
    {
       
    }

}

abstract public class ChracterStateRun : ChracterState
{
    public float RunSpeed = 2000;
    
    public ChracterStateRun(ChracterStateOwner StateOwner) : base(StateOwner)
    {
    }

    public override void Enter()
    {
        base.Enter();
        CharacterAnimator().SetTrigger("TrRun");
    }

    public override void Jump()
    {
        Owner.ChangeState(ChState.Jump);
        base.Jump();
    }

    public override void Exit()
    {
        base.Exit();
        CharacterAnimator().ResetTrigger("TrRun");
    }

    protected virtual float Multiplier()
    {
        return 1;
    }

    public override void Update()
    {
        float force = Multiplier() * RunSpeed;
        Rigidbody2D body = Owner.Character().GetComponent<Rigidbody2D>();
        body.AddForce(new Vector2(force, 0), ForceMode2D.Force);
        base.Update();
    }

    public override void StopMove()
    {
        Owner.ChangeState(ChState.Idle);
        base.StopMove();
    }

}

public class ChracterStateRunLeft : ChracterStateRun
{
    public ChracterStateRunLeft(ChracterStateOwner StateOwner) : base(StateOwner)
    {
    }

    protected override float Multiplier()
    {
        return -1;
    }

    public override void MoveRight()
    {
        StopMove();
    }

    public override void Enter()
    {
        base.Enter();
        CharacterAnimator().SetBool("DirrectionRight", false);
    }
    
}

public class ChracterStateRunRight : ChracterStateRun
{
    public ChracterStateRunRight(ChracterStateOwner StateOwner) : base(StateOwner)
    {
    }

    public override void MoveLeft()
    {
        StopMove();
    }

    public override void Enter()
    {
        base.Enter();
        CharacterAnimator().SetBool("DirrectionRight", true);
    }

}

public class ChracterStateIdle : ChracterState
{
    public ChracterStateIdle(ChracterStateOwner StateOwner) : base(StateOwner)
    {
    }

    public override void Enter()
    {
        base.Enter();
        CharacterAnimator().SetTrigger("Stay");
    }

    public override void Jump()
    {
        Owner.ChangeState(ChState.Jump);
        base.Jump();
    }

    public override void MoveLeft()
    {
        Owner.ChangeState(ChState.RunLeft);
        base.MoveLeft();
    }

    public override void MoveRight()
    {
        Owner.ChangeState(ChState.RunRight);
        base.MoveLeft();
    }

    public override void Exit()
    {
        base.Exit();
        CharacterAnimator().ResetTrigger("Stay");
    }
}

public class ChracterStateInAir : ChracterState
{
    public int JumpsLeft = 3;
    public float SlideMultiplier = 0;
    public float SlideForce = 20000;
    public ChracterStateInAir(ChracterStateOwner StateOwner) : base(StateOwner)
    {
        
    }


    public override void Enter()
    {
        base.Enter();
        Owner.Character().GetComponent<Animator>().SetBool("IsInAir", true);
    }

    public override void Jump()
    {

        Rigidbody2D body = Owner.Character().GetComponent<Rigidbody2D>();
        if (body.velocity.y < 0)
        {
            body.velocity = new Vector2(body.velocity.x, 0);
            body.angularVelocity = 0;
        }

        MonoBehaviour.print("TrAirJump");

        Owner.ChangeState(ChState.Jump);

        base.Jump();
    }

    public override void Grounded()
    {
        Owner.ChangeState(ChState.Idle);
        base.Grounded();
    }


    public override void MoveLeft()
    {
        CharacterAnimator().SetBool("DirrectionRight", false);
        SlideMultiplier = -1;
        base.MoveLeft();
    }

    public override void MoveRight()
    {
        CharacterAnimator().SetBool("DirrectionRight", true);
        SlideMultiplier = 1;
        base.MoveRight();
    }

    public override void StopMove()
    {
        SlideMultiplier = 0;
        base.StopMove();
    }

    public override void Update()
    {
        float airSpeed = SlideForce * SlideMultiplier;
        Vector2 vec = new Vector2(airSpeed, 0);
      
        Rigidbody2D body = Owner.Character().GetComponent<Rigidbody2D>();
        body.AddForce(vec, ForceMode2D.Force);
        base.Update();
    }

    public override void WallCollided(bool isCollided)
    {
        if (isCollided)
        {
            Owner.ChangeState(ChState.WallGlide);
        }

        base.WallCollided(isCollided);
    }


    public override void Exit()
    {
        Owner.Character().GetComponent<Animator>().SetBool("IsInAir", false);
        base.Exit();
    }
}


public class ChracterStateJump : ChracterState
{
    public ChracterStateJump(ChracterStateOwner StateOwner) : base(StateOwner)
    {
    }

    public override void Enter()
    {
        CharacterAnimator().SetTrigger("TrJump");
        CharacterAnimator().SetBool("JumpStarted", true);
        base.Enter();
    }

    public override void Exit()
    {
        Rigidbody2D body = Owner.Character().GetComponent<Rigidbody2D>();
        Vector2 vectorBefore = body.velocity;
        float thrust = 60000;

        body.AddForce(new Vector2(vectorBefore.x * 10, thrust), ForceMode2D.Force);
        
        CharacterAnimator().ResetTrigger("TrJump");
        CharacterAnimator().SetBool("JumpStarted", false);
       
        base.Exit();
    }

}

public class ChracterStateWallGlide : ChracterState
{
    private float gravityCompensation = 1200;
    public ChracterStateWallGlide(ChracterStateOwner StateOwner) : base(StateOwner)
    {
    }

    public override void Enter()
    {
        CharacterAnimator().SetTrigger("TrSlideWall");
        base.Enter();
    }

    public override void Jump()
    {
        Owner.ChangeState(ChState.Jump);
        base.Jump();
    }

    public override void Update()
    {
        Rigidbody2D body = Owner.Character().GetComponent<Rigidbody2D>();
        Vector2 vec = new Vector2(0, gravityCompensation);
        body.AddForce(vec, ForceMode2D.Force);

        base.Update();
    }

    public override void WallCollided(bool isCollided)
    {
        if (!isCollided)
        {
            Owner.ChangeState(ChState.InAir);
        }
        base.WallCollided(isCollided);
    }

    public override void Grounded()
    {
        Owner.ChangeState(ChState.Idle);
        base.Grounded();
    }

    public override void Exit()
    {
        CharacterAnimator().ResetTrigger("TrSlideWall");

        base.Exit();
    }

}